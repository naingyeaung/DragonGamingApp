<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home/', 'CustomerController@index');
Route::get('/products/', 'CustomerController@productsIndex');
Route::get('/promotions/', 'CustomerController@promotionIndex');
Route::get('/productscate/{id}','CustomerController@ProductsCategory');
Route::get('/details/{id}', 'CustomerController@view');
Route::post('/details/','CustomerauthController@addfeedback');
Route::post('/search/','CustomerController@search');
Route::post('cart/add', 'CustomerauthController@addtocart');
Route::get('cart/', 'CustomerauthController@showcart');
Route::get('cart/reducecart/{id}', 'CustomerauthController@reducecart');
Route::get('cart/removecart/{id}', 'CustomerauthController@removecart');
Route::post('order/', 'CustomerauthController@order');
Route::get('/verifyemail/{token}', 'CustomerauthController@verify');
	
	
Route::get('admin/', 'ProductController@index');
	
Route::get('admin/categories/', 'CategoriesController@index');
Route::get('admin/categories/view/{id}', 'CategoriesController@view');

Route::get('admin/categories/add', 'CategoriesController@add');
Route::post('admin/categories/add', 'CategoriesController@create');

Route::get('admin/categories/delete/{id}', 'CategoriesController@delete');

Route::get('admin/categories/edit/{id}', 'CategoriesController@edit');
Route::post('admin/categories/edit/{id}', 'CategoriesController@update');



Route::get('admin/brands/', 'BrandController@index');
Route::get('admin/brands/view/{id}', 'BrandController@view');

Route::get('admin/brands/add', 'BrandController@add');
Route::post('admin/brands/add', 'BrandController@create');

Route::get('admin/brands/delete/{id}', 'BrandController@delete');

Route::get('admin/brands/edit/{id}', 'BrandController@edit');
Route::post('admin/brands/edit/{id}', 'BrandController@update');



Route::get('admin/products/', 'ProductController@index');
Route::get('admin/products/view/{id}', 'ProductController@view');

Route::get('admin/products/add', 'ProductController@add');
Route::post('admin/products/add', 'ProductController@create');

Route::get('admin/products/delete/{id}', 'ProductController@delete');

Route::get('admin/products/edit/{id}', 'ProductController@edit');
Route::post('admin/products/edit/{id}', 'ProductController@update');

Route::post('admin/products/view/{id}', 'ProductController@status');




Route::get('admin/customizes/', 'CustomizeController@index');
Route::get('admin/customizes/uploadProduct/{specified}/{id}', 'CustomizeController@uploadProduct');
Route::get('admin/customizes/uploadBrand/{specified}/{id}', 'CustomizeController@uploadBrand');

Auth::routes();


