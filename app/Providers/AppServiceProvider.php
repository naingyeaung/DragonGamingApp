<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Category;
use App\Product;
use App\Brand;
use App\Cart;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $count=0;
        Schema::defaultStringLength(191);
        View::share('count_category', Category::all()->count());
        View::share('categories', Category::all());
        View::share('brands', Brand::all());
        View::share('products', Product::all());
		View::share('count_product', Product::all()->count());
        View::share('count_brand', Brand::all()->count());
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
