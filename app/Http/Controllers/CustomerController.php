<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Brand;
use App\Category;

class CustomerController extends Controller
{
    //
    public function index()
	{
		$products = Product::all();
		$brands = Brand::all();
		$categories = Category::all();
		$titleProduct = Product::where('specified','title')->first();
		$slide1Product = Product::where('specified','slide1')->first();
		$slide2Product = Product::where('specified','slide2')->first();
		$slide3Product = Product::where('specified','slide3')->first();
		$rec1Product = Product::where('specified','rm1')->first();
		$rec2Product = Product::where('specified','rm2')->first();
		$rec3Product = Product::where('specified','rm3')->first();
		$rec4Product = Product::where('specified','rm4')->first();
		$best1Product = Product::where('specified','bs1')->first();
		$best2Product = Product::where('specified','bs2')->first();
		$brand1Product = Brand::where('specified','b1')->first();
		$brand2Product = Brand::where('specified','b2')->first();
		$brand3Product = Brand::where('specified','b3')->first();
		
				
		return view ('home', ['products'=>$products,'brands'=>$brands,'categories'=>$categories, 'titleProduct'=>$titleProduct, 'slide1Product'=>$slide1Product, 'slide2Product'=>$slide2Product, 'slide3Product'=>$slide3Product, 'rec1Product'=>$rec1Product, 'rec2Product'=>$rec2Product, 'rec3Product'=>$rec3Product, 'rec4Product'=>$rec4Product, 'best1Product'=>$best1Product, 'best2Product'=>$best2Product, 'brand1Product'=>$brand1Product, 'brand2Product'=>$brand2Product, 'brand3Product'=>$brand3Product]);
		
	}
	public function productsIndex()
	{
		$products = Product::all();
		return view('customerview/index', ['products'=>$products]);	
	}
	public function productsCategory($id)
	{
		$products = Product::where('category_id', $id)->get();
		return view('customerview/index', ['products'=>$products]);	
	}
	public function promotionIndex()
	{	
		$discountedCount = Product::where('status', 1)->count();
		$discountedProducts = Product::where('status', 1)->get();	
		$promotedProducts = Product::where('status', 2)->get();
		return view ('customerview/promotion', ['discountedProducts'=>$discountedProducts, 'promotedProducts'=>$promotedProducts, 'discountedCount'=>$discountedCount]);
	}
	public function search()
	{
		$type = request()->forType;
		$id = request()->forid;
		
		if($type=='consoles')
		{
		$product = Product::find($id);
		$features = Product::where('category_id', $product->category_id)->where('id','<>',$id)->get();
		return view('customerview/detail',['product'=>$product, 'features'=>$features]);

		}
		else if($type=='categories')
		{
			$products = Product::where('category_id', $id)->get();
		return view('customerview/index', ['products'=>$products]);
		}
		else if($type=='brands')
		{
			$products = Product::where('brand_id', $id)->get();
		return view('customerview/index', ['products'=>$products]);
		}
		else{
			
		}
		
	}
	public function view($id)
	{
		$product = Product::find($id);
		$features = Product::where('category_id', $product->category_id)->where('id','<>',$id)->get();
		return view('customerview/detail',['product'=>$product, 'features'=>$features]);
	}
}
