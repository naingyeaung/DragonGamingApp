<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Purchasement;
use App\Purchasementdetail;
use App\Feedback;
use Session;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Jobs\SendVerificationEmail;

class CustomerauthController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('checkRole:0');
		$this->middleware('auth');
    }
    public function addtocart()
    {
        $id = request()->id;
        $qty = request()->qty;
        $b = false;
        $carts = session()->get('cart');
        $newcart = [];
        $item = [
            'id' => $id,
            'qty' => $qty
          ];
        if($carts==[])
        {
            Session::push('cart', $item);
            $b=true;
        }
        else
        {
            foreach($carts as $cart)
            {
                if($cart["id"]==$id)
                    {
                        $cart["qty"] += $qty;
                        $newcart[] = [
                            'id'=>$cart['id'],
                            'qty' => $cart['qty']
                        ];
                        $b=true;
                    }
                else
                    {
                        $newcart[] = [
                            'id'=>$cart['id'],
                            'qty' => $cart['qty']
                        ];
                    }  
           
            }
            Session::put('cart', $newcart);
            if($b==false)
            {
                Session::push('cart', $item);
            }
        }
        return back();
        
    }

    public function showcart()
    {
        $carts = session()->get('cart');
        $productsfromcarts = new Collection();
        foreach($carts as $cart)
        {
            $products = Product::where("id", $cart["id"])->get();
            $productsfromcarts = $productsfromcarts->merge($products);
        }
        return view('customerview/cart',['products'=>$productsfromcarts]);
    }

    public function reducecart($id)
    {
        $carts = session()->get('cart');
        $newcart = [];
        foreach($carts as $cart)
            {
                if($cart["id"]==$id)
                    {
                        if($cart["qty"]==1)
                        {

                        }
                        else
                        {
                        $cart["qty"] -= 1;
                        $newcart[] = [
                            'id'=>$cart['id'],
                            'qty' => $cart['qty']
                        ];
                        }
                        $b=true;
                    }
                else
                    {
                        $newcart[] = [
                            'id'=>$cart['id'],
                            'qty' => $cart['qty']
                        ];
                    }  
           
            }
            Session::put('cart', $newcart);
            return back();
    }

    public function removecart($id)
    {
        $carts = session()->get('cart');
        $newcart = [];
        foreach($carts as $cart)
            {
                if($cart["id"]==$id)
                    {
                        
                        
                        
                        
                    }
                else
                    {
                        $newcart[] = [
                            'id'=>$cart['id'],
                            'qty' => $cart['qty']
                        ];
                    }  
           
            }
            Session::put('cart', $newcart);
            return back();
    }


        /**

    * Handle a registration request for the application.

    *

    * @param \Illuminate\Http\Request $request

    * @return \Illuminate\Http\Response

    */
    public function order()
    {
        $carts = session()->get('cart');
        $order = new Purchasement();
        $order->user_id = auth::user()->id;
        $order->phone = request()->phone;
        $order->address = request()->daddress;
        $order->subtotal = request()->subtotal;
        $order->dcharges = request()->dcharges;
        $order->totalamount = request()->total;
        $order->save();
        $ord = Purchasement::find($order->id);
        $ord->email_token = base64_encode($order->id);
        $ord->save();

        foreach($carts as $cart)
        {
            $product = Product::find($cart['id']);
            $orderdetail = new Purchasementdetail();
            $orderdetail->purchasement_id = $order->id;
            $orderdetail->product_id = $cart["id"];
            $orderdetail->qty = $cart["qty"];
            $orderdetail->totalprice = $product->price * $cart["qty"];
            $orderdetail->save();

        }
        dispatch(new SendVerificationEmail($order));
        
        return view('verification');

    }

    /**

    * Handle a registration request for the application.

    *

    * @param $token

    * @return \Illuminate\Http\Response

    */

    public function verify($token)

    {

    $order = Purchasement::where(‘email_token’,$token)->first();

    $order->verified = 1;

    if($order->save()){

    return view(‘emailconfirm’,[‘order’=>$order]);

    }

    }

    public function addfeedback()
    {
        $feedback = new Feedback();
        $feedback->user->id = auth::user()->id;
        $feedback->product_id = request()->product_id;
        $feedback->comment = request()->feedback;
        
        $feedback->save();

        return back();
    }
}
