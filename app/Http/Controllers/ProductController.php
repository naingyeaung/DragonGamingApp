<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Brand;


class ProductController extends Controller
{
    //
    public function __construct()
	{
		$this->middleware('checkRole:1');
		$this->middleware('auth');
		
	}
	public function index()
	{
		$products = Product::paginate(5);
		return view('product/index', ['products'=>$products]); 
	}
	public function view($id)
	{
		$product = Product::find($id);
		return view('product/view', ['product'=>$product]); 
	}
	
	public function add()
	{
		return view('product/add',['categories'=>Category::all(), 'brands'=>Brand::all()]);
	}
	public function create()
	{
		 $validator = validator(request()->all(),[
        'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    	]);
		if ($validator->fails()) {
        return redirect('admin/products/add')->witherrors($validator);
      	}	

		$product = new Product();
		$product->name = request()->name;
		$product->description = request()->description;
		$product->price = request()->price;
		$product->category_id = request()->category_id;
		$product->brand_id = request()->brand_id;
		$product->status = request()->status;
		$product->dp = request()->dp;
		$imageName = time().'.'.request()->photo->getClientOriginalExtension();
		request()->photo->move(public_path('images'), $imageName);
		$product->photo = $imageName;
		$product->save();
		return redirect('admin/products/');
	}
		public function edit($id)
	{
		$product = Product::find($id);
		return view('product/edit',['product'=>$product, 'categories'=>Category::all(), 'brands'=>Brand::all()]);
	}
	public function update($id)
	{
		$product = Product::find($id);
		$product->name = request()->name;
		$product->description = request()->detail;
		$product->price = request()->price;
		$product->category_id = request()->category_id;
		$product->brand_id = request()->brand_id;
		$product->status = $product->status;
		$product->dp = $product->dp;
		$product->photo = $product->photo;
		$product->save();
		return redirect('admin/products/');
	}
	public function delete($id)
	{
				$product = Product::find($id);
		$product->delete();
		return redirect('/admin/')->with('info','Category Deleted');
		}
	
	public function status($id)
	{
		$product = Product::find($id);
		$product->status = request()->status;
		$product->dp = request()->dp;
		$product->save();
		return back();
	}
}
