<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;

class BrandController extends Controller
{
    //
    public function __construct()
	{
		$this->middleware('checkRole:1');
		$this->middleware('auth');
		
	}
	public function index(){
		
		$brands = Brand::all();
		return view('brand/index', ['brands' => $brands]);
		}
	public function view($id)
	{
		$brand = brand::find($id);
		return view('brand/view', ['brand' => $brand])	;
	}	
	public function add(){
      return view('brand/add');
    }

    public function create(){
		
    $validator = validator(request()->all(),[
        'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    	]);
		if ($validator->fails()) {
        return redirect('admin/brands/add')->witherrors($validator);
      	}	
      $brand = new Brand();
	  $brand->name = request()->name;
	  $imageName = time().'.'.request()->photo->getClientOriginalExtension();
		request()->photo->move(public_path('images'), $imageName);
		$brand->photo = $imageName;
      $brand->save();
      return redirect('/admin/brands/')->with('info', 'Brand Added');/*redirect က route ျဖစ္တယ္*/
    }
	
	public function edit($id){
		$brand = Brand::find($id);
		return view('brand/edit', ['brand' => $brand]);
		}
		
	public function update($id){
		$validator = validator(request()->all(),[
        "name"=>"required | min:3",
      
      ]);
      if ($validator->fails()) {
        return redirect('admin/brand/edit')->witherrors($validator);
      }
	  $brand = Brand::find($id);
	  $brand->name = request()->name;
	  $brand->save();
	  return redirect('/admin/brands/')->with('info', 'Brand Edited');
		}	
	public function delete($id){
		
		$brand = Brand::find($id);
		$brand->delete();
		return redirect('/admin/brands/')->with('info','Brand Deleted');
		}
}
