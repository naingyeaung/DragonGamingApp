<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Brand;

class CustomizeController extends Controller
{
    //
    public function __construct()
	{
		$this->middleware('checkRole:1');
		$this->middleware('auth');
		
	}
	public function index()
	{
		$products = Product::all();
		$brands = Brand::all();
		$titleProduct = Product::where('specified','title')->first();
		$slide1Product = Product::where('specified','slide1')->first();
		$slide2Product = Product::where('specified','slide2')->first();
		$slide3Product = Product::where('specified','slide3')->first();
		$rec1Product = Product::where('specified','rm1')->first();
		$rec2Product = Product::where('specified','rm2')->first();
		$rec3Product = Product::where('specified','rm3')->first();
		$rec4Product = Product::where('specified','rm4')->first();
		$best1Product = Product::where('specified','bs1')->first();
		$best2Product = Product::where('specified','bs2')->first();
		$brand1Product = Brand::where('specified','b1')->first();
		$brand2Product = Brand::where('specified','b2')->first();
		$brand3Product = Brand::where('specified','b3')->first();
		
				
		return view ('customize/customize', ['products'=>$products, 'brands'=>$brands, 'titleProduct'=>$titleProduct, 'slide1Product'=>$slide1Product, 'slide2Product'=>$slide2Product, 'slide3Product'=>$slide3Product, 'rec1Product'=>$rec1Product, 'rec2Product'=>$rec2Product, 'rec3Product'=>$rec3Product, 'rec4Product'=>$rec4Product, 'best1Product'=>$best1Product, 'best2Product'=>$best2Product, 'brand1Product'=>$brand1Product, 'brand2Product'=>$brand2Product, 'brand3Product'=>$brand3Product]);	
	}
	
	public function uploadProduct($specified,$id)
	{
			$oldProduct = Product::where('specified',$specified)->first();
			if($oldProduct){
				$oldProduct->specified = '';
				$oldProduct->save();
				}
			$newProduct = Product::find($id);
			$newProduct->specified = $specified;
			$newProduct->save();
			return redirect('admin/customizes/');	
	}
	
	public function uploadBrand($specified,$id)
	{
			$oldBrand = Brand::where('specified',$specified)->first();
			if($oldBrand){
				$oldBrand->specified = '';
				$oldBrand->save();
				}
			$newBrand = Brand::find($id);
			$newBrand->specified = $specified;
			$newBrand->save();
			return redirect('admin/customizes/');	
	}	

}
