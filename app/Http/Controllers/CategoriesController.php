<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{
    //
    public function __construct()
	{
		$this->middleware('checkRole:1');
		$this->middleware('auth');
		
	}
	public function index(){
		
		$categories = Category::all();
		return view('category/index', ['categories' => $categories]);
		}
	public function view($id)
	{
		$category = Category::find($id);
		return view('category/view', ['category' => $category])	;
	}	
	public function add(){
      return view('category/add');
    }

    public function create(){
      $validator = validator(request()->all(),[
        "name"=>"required | min:3",
      
      ]);
      if ($validator->fails()) {
        return redirect('admin/category/add')->witherrors($validator);
      }
      $category = new Category();
	  $category->name = request()->name;
      $category->save();
      return redirect('/admin/categories/')->with('info', 'Category Added');
    }
	
	public function edit($id){
		$category = Category::find($id);
		return view('category/edit', ['category' => $category]);
		}
		
	public function update($id){
		$validator = validator(request()->all(),[
        "name"=>"required | min:3",
      
      ]);
      if ($validator->fails()) {
        return redirect('admin/category/edit')->witherrors($validator);
      }
	  $category = Category::find($id);
	  $category->name = request()->name;
	  $category->save();
	  return redirect('/admin/');
		}	
	public function delete($id){
		
		$category = Category::find($id);
		$category->delete();
		return redirect('/admin/categories')->with('info','Category Deleted');
		}	
}
