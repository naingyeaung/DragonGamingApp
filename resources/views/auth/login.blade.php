@extends('layouts.customerview')

@section('content')
<div class="loginseperation"></div>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="card card-default" style="background-color:black;color:white;">
                <div class="card-header"><h3>Login</h3></div>

                <div class="card-body">
                <div class="row" style="height:280px;">
                 <div class="col-md-6" style="height:280px;">
                    <img id="logo" src="images/logo.jpg" width="80%" height="80%" />
                    </div>
                <div class="col-md-6">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn btn-default" style="background-color:#BB0404;color:white">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                   <font size="2"> Forgot Your Password? </font>
                                </a>
                            </div>
                        </div>
                    </form>
                    </div>
                   
                    
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="loginseperation"></div>
@endsection

