@extends('layouts/customerview')

@section('content')
<div class="separation">
</div>


<div class="container-fluid">
<div class="row">
@if($products)
@foreach($products as $product)
<div class="col-md-3" style="height:620px;" align="center">
<div class="product-card2" align="center">

<img src="{{ URL::to('/') }}/images/{{ $product->photo ?? '' }}" width="100%" height="52%" />
<br />

<div style="width:90%;" align="left">

<div class="product-title" align="center">
<font size="3" color="#CCCCCC">{{$product->name}}</font><br />
</div>

<div class="product-price">
@if($product->status==1)
<b><font size="4" color="white" style="text-decoration:line-through;">Kyats {{$product->price}}/piece</font></b><br /><br />
<div class="badge" style="background-color:#0f0;">Dis</div><b><font size="4" color="white" > {{$product->dp}}ks</font></b><br />
@elseif($product->status==2)
<b><font size="4" color="white">Kyats {{$product->price}}/piece</font></b><br /><br />
<b><div class="badge" style="background-color:#00A0EE;">Pro</div><font size="4" color="white"> {{$product->dp}}</font></b><br />
@else
<b><font size="4" color="white" >Kyats {{$product->price}}/piece</font></b><br /><br />
<div class="badge" style="background-color:#BB0404;color:white;" >Auth</div><b><font size="4" color="white" > Authentic Product </font></b>
@endif


@if($product->specifed=='bs1' || $product->specified=='bs2')
<div class="badge badge-primary" >Best</div><b><font size="4" color="white" > Best Seller </font></b>
@endif
</div>

<form method="post" action="{{url('cart/add')}}"  >
{{ csrf_field() }}
<input type="hidden" value="{{$product->id}}" name="id"/>
<input type="hidden" value="1" name="qty"/>
<button type="submit" class="btn" style="background-color:#FF8000; color:white;"><i class="fas fa-cart-plus"></i> Add To Cart</button>



<a href="{{url("/details/$product->id")}}"><div class="btn" 
style="background-color:#0099CC; color:white;"><i class="fas fa-gamepad"></i> Product Details</div></a>   
</form>
</div>

</div>
</div>
@endforeach
@endif
</div>
</div>

<div class="separation">
</div>

@endsection