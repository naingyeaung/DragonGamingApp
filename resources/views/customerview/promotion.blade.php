@extends('layouts/customerview')

@section('content')
<div class="separation">
</div>


<div class="container">


@foreach($discountedProducts as $discountedProduct)
<div class="row" style="">
<div class="col-md-5" style="height:340px;" align="left">

<img src="{{ URL::to('/') }}/images/{{ $discountedProduct->photo ?? '' }}" width="90%" height="100%" />
<br />
</div>

<div class="col-md-7">
<br>
<font size="5" color="#CCCCCC"> <div class="badge badge-secondary"> QA</div> {{$discountedProduct->name}}</font>
<br>
<font color="#999">
{{$discountedProduct->description}}
</font>
<br>
<br>
<b><font size="4" color="white" style="text-decoration:line-through;">Kyats {{$discountedProduct->price}}/piece</font></b><br />
<div class="badge" style="background-color:#0f0;">Dis</div><b><font size="4" color="white" > {{$discountedProduct->dp}}ks</font></b><br />
@if($discountedProduct->specifed=='bs1' || $discountedProduct->specified=='bs2')
<div class="badge badge-primary" >Best</div><b><font size="4" color="white" > Best Seller </font></b>
@endif

<br>

<button class="btn" style="background-color:#FF8000; color:white;"><i class="fas fa-cart-plus"></i> Add To Cart</button>
<a href="{{url("details/$discountedProduct->id")}}"><button class="btn" style="background-color:#0099CC; color:white;"><i class="fas fa-gamepad"></i> Product Details</button></a>

</div>

<br />


</div>
<hr>
@endforeach


<div class="separation">
</div>



@foreach($promotedProducts as $promotedProduct)
<div class="row" style="">
<div class="col-md-5" style="height:320px;">

<img src="{{ URL::to('/') }}/images/{{ $promotedProduct->photo ?? '' }}" width="90%" height="100%" />
<br />

</div>

<div class="col-md-7">
<br>
<font size="5" color="#CCCCCC"><div class="badge badge-secondary"> QA</div> {{$promotedProduct->name}}</font><br />
<font color="#999">
{{$promotedProduct->description}}
</font>
<br><br>
<b><font size="4" color="white">Kyats {{$promotedProduct->price}}/piece</font></b><br />
<b><div class="badge" style="background-color:#00A0EE;">Pro</div><font size="4" color="white"> {{$promotedProduct->dp}}</font></b><br /><br>
@if($promotedProduct->specifed=='bs1' || $promotedProduct->specified=='bs2')
<div class="badge badge-primary" >Best</div><b><font size="4" color="white" > Best Seller </font></b>
@endif


<button class="btn" style="background-color:#FF8000; color:white;"><i class="fas fa-cart-plus"></i> Add To Cart</button>
<a href="{{url("details/$promotedProduct->id")}}"><button class="btn" style="background-color:#0099CC; color:white;"><i class="fas fa-gamepad"></i> Product Details</button></a>

</div>


</div>
<hr>
@endforeach

</div>

<div class="separation">
</div>

@endsection