@extends('layouts/customerview')

@section('content')

<div class="separation"></div>

<div class="container">
    <div class="row">
        <div class="col-md-5">
        <div style="border-style:solid;width:80%;height:320px;border-color:white;border-width:1px;vertical-align:middle;border-radius:5px;"
         class="d-flex justify-content-center align-items-center">
        <img src="{{asset("images/$product->photo")}}" width="80%" height="80%"  />
        </div>
        <div class="priceseperation"></div>
        <div style="width:80%;color:white;">
        <h4 style="color:#c52121;"><b> Description </b></h4>
        <p>The Razer Blade Stealth laptop is crafted to deliver incredible performance in an impossibly thin form factor. Powered by an Intel® Core™ i7 processor,
         the Razer Blade Stealth lets you perform at your best, wherever you choose to be.  </p>
        </div>
        </div>
        <div class="col-md-7" style="color:white;" align="left">
        <h1>{{$product->name}} Authentic</h1>
            <div style="width:100%" align="">
                <p>Brand:{{$product->brand->name}} , Category:{{$product->category->name}}</p>
            </div>
        <font color="#FFDF00">
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        </font>
        &nbsp;
         Total 128 User Reviews
            <div style="width:80%" align="left">
            <hr style="border-color:white;">
            @php $price = 0;
            @endphp
            @if($product->status==0)
                <h2>MMK {{$product->price}}</h2>
                @php $price = $product->price;
            @endphp
            @elseif($product->status==1)
            <h2 style="color:" ><div class="badge" style="background-color:#0f0;color:black;">Dis</div>  MMK {{$product->dp}}</h2>
            @php $price = $product->dp;
            @endphp
            @endif
            @if($product->status==2)
            <h2>MMK {{$product->price}}</h2>
            <h2 style="color:"><div class="badge" style="background-color:#00A0EE;">Pro</div> {{$product->dp}}</h2>
            @php $price = $product->price;
            @endphp
            @endif    
            <hr style="border-color:white;">
            <form  method="post" action="{{url('cart/add')}}">
            {{ csrf_field() }}
            <input type="hidden" value="{{$product->id}}" name="id"/>
            <h4>Quantity</h4>
            <select class="form-control" id="qtySelect" onchange="changeQty()" name="qty">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            </select>
            <div class="priceseperation"></div>
            @if($product->status==1)
           
            <h4 style="color:" id="price">Total Price : MMK {{$product->dp}}</h4>
            @else
            
            <h4 style="color:" id="price">Total Price : MMK {{$product->price}}</h4>
            @endif
            <br>
            <button type="submit" class="btn" style="background-color:#c52121; color:white;"><i class="fas fa-cart-plus"></i> Add To Cart</button>
            </form>
            </div>    
        </div>
        
    </div>
    <hr style="border-color:white;">
    <div class="row">
    
        <div class="col-md-5" style="color:#CCC" >
            <h2 style="color:#c52121;"><b>Reviews from Customers</b></h2>
         <hr>
            <div style="width:80%">
         <h4><b>Naing Ye Aung</b></h4>
         <font color="#FFDF00">
         <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        </font><br>
        <font color="#CCC" size="2">{{$product->created_at->diffForHumans()}}</font>
        <br><br>
        <p>The Razer Blade Stealth laptop is crafted to deliver incredible performance in an impossibly thin form factor. Powered by an Intel® Core™ i7 processor,
         the Razer Blade Stealth lets you perform at your best</p>
         <hr style="border-color:white;">
            </div>
            <div style="width:80%">
         <h4><b>Sai Htut Naing</b></h4>
         <font color="#FFDF00">
         <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        </font><br>
        <font color="#CCC" size="2">{{$product->created_at->diffForHumans()}}</font>
        <br><br>
        <p>The Razer Blade Stealth laptop is crafted to deliver incredible performance in an impossibly thin form factor. Powered by an Intel® Core™ i7 processor,
         the Razer Blade Stealth lets you perform at your best</p>
         <hr style="border-color:white;">
            </div>
            <div style="width:80%">
         <h4><b>Ag Kg Myat</b></h4>
         <font color="#FFDF00">
         <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        </font><br>
        <font color="#CCC" size="2">{{$product->created_at->diffForHumans()}}</font>
        <br><br>
        <p>The Razer Blade Stealth laptop is crafted to deliver incredible performance in an impossibly thin form factor. Powered by an Intel® Core™ i7 processor,
         the Razer Blade Stealth lets you perform at your best</p>
         <hr style="border-color:white;">
            </div>
        </div>
        <div class="col-md-7" style="color:white;">
        <h3 style="color:#c52121;">Feedback</h3>
        <hr>
        <h5>Rate This Product</h5>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <i class="fas fa-star" aria-hidden="true"></i>
        <hr>
        <h5>Give Feedback</h5>
        <form method="post">
            <textarea class="form-control" name="feedback"></textarea>
            <input type="hidden" name="product_id" value="{{$product->id}}">
            <br>
            <button class="btn" style="background-color:#c52121; color:white;"><i class="fas fa-envelope"></i> Submit</button>
        </form>
        <hr>
        <h3>Featured Products</h3>
        <hr>
        @foreach($features as $feature) 
        <div class="row">
            <div class="col-md-5" style="height:350px;">
                <img src="{{asset("images/$feature->photo")}}" width="100%"  class="features-photo"/>
                
            </div>
            <div class="col-md-7">
                <h3 style="color:white"><b>{{$feature->name}}</b></h3>
                <h5>MMK {{$feature->price}}</h4>
                <font color="#FFDF00">
                <i class="fas fa-star" aria-hidden="true"></i>
                <i class="fas fa-star" aria-hidden="true"></i>
                <i class="fas fa-star" aria-hidden="true"></i>
                <i class="fas fa-star" aria-hidden="true"></i>
                <i class="fas fa-star" aria-hidden="true"></i>
                </font>
                <p>156 reviews</p>
                <a href="{{url("details/$feature->id")}}"><button class="btn" style="background-color:#c52121; color:white;"><i class="fas fa-gamepad"></i> Product Details</button></a>
                <hr>
            </div>
        </div> 
        @endforeach  
        </div>
        
    </div>
  
</div>
<div class="separation"></div>

@endsection
<script>
function changeQty()
{
    var qty = $("#qtySelect").val();
    var price = {!! json_encode($price) !!};
    var total = qty * price;
    $("#price").html('Total Price : MMK ' + total);

}
</script>