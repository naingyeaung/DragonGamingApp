@extends('layouts/customerview')

@section('content')
<div class="separation"></div>
    <div class="container">
        <div class="row">
        <div class="col-md-8" style="color:#999">
            <h2 align="right" style="color:white">Hello Naing Ye Aung</h2>
            <div class="badge" style="width:100%;height:60px;line-height:60px;background-color: black;color:#c52121;text-align:left;text-indent:5%"><h2>Shopping Cart</h2></div>
            <div class="separation"></div>
            @php
            $total=0;
            @endphp
            @foreach($products as $product)
                
                <div class="row" align="center">
                    <div class="col-md-3" style="height:200px"><img src="{{asset("images/$product->photo")}}" width="95%" height="80%"  /></div>
                    <div class="col-md-3"><h4 style="color:white">{{$product->name}}</h4></div>
                    <div class="col-md-3">
                        @foreach(Session::get('cart') as $cart)
                            @if($product->id==$cart['id'])
                           @php
                           $cartid = $cart['id'];
                           @endphp
                           <form class="form-inline" method="post" action="{{url('cart/add')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$cart['id']}}" />
                            <input type="hidden" name="qty" value="1" /><h5 style="color:white">
                            <a href="{{url("cart/reducecart/$cartid")}}"><div class="btn" style="background-color:transparent;border-color:transparent;"><h5><i style="color:white;" class="fas fa-minus-square"></i></h5></div></a>  {{$cart['qty']}} 
                            <button type="submit" class="btn" style="background-color:transparent;border-color:transparent;"><h5><i style="color:white;" class="fas fa-plus-square"></i></h5></button>
                            </form>
                            <br><br>
                            <a href="{{url("cart/removecart/$cartid")}}" style="color:white"><i class="fas fa-trash-alt" ></i></a>
                            </h5>
                            @endif
                        @endforeach
                    </div>
                    <div class="col-md-3">
                    @foreach(Session::get('cart') as $cart)
                            @if($product->id==$cart['id'])
                                <h4 style="color:white">MMK {{$product->price*$cart['qty']}}</h4>
                                @php
                                $total +=$product->price*$cart['qty'];
                                @endphp
                            @endif
                    @endforeach
                    </div>
                </div>
                
            @endforeach
            <hr style="border-color:white;">
            <div class="badge" style="width:100%;height:50px;line-height:60px;background-color: black;text-align:left;text-indent:5%"><h4 style="color:white;">Satisfied With the Purchase?</h4></div>
            <br><br>
            Prices and offers in the cart are subject to change until the order is submitted.
            <br><br>
            <hr style="border-color:white;">
        </div>
        <div class="col-md-4" style="color:#999">
            <div style="background-color:black;width:90%;" align="center">
                <div style="width:90%;" >
                    <br>
                    <h3 align="left">Cart Summary</h3>
                    <hr style="border-color:white;">
                    <br>
                    <table width="100%">
                        <tr>
                            <td width="50%" align="left">
                                <h5 style="color:white">SubTotal</h5>
                            </td>
                            <td width="50%" align="right">
                                <h5 style="color:white">MMK {{$total}}</h5>
                            </td>
                        </tr>
                        <tr>
                                <td width="50%" align="left">
                                    <h6 style="color:#666">Delivery</h6>
                                </td>
                                <td width="50%" align="right">
                                <h6 style="color:#666">MMK 2500</h6>
                                </td>
                        </tr>
                        <tr>
                                <td width="50%" align="left">
                                    <h6 style="color:#666">Tax</h6>
                                </td>
                                <td width="50%" align="right">
                                <h6 style="color:#666"> MMK {{($total*5)/100}}</h6>
                                </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table width="100%">
                        <tr>
                            <td width="50%" align="left">
                                <h3 style="color:white">Total</h4>
                            </td>
                            <td width="50%" align="right">
                                <h4 style="color:white">MMK {{$total+(($total*5)/100)+2500}}</h4>
                            </td>
                        </tr>
                    </table>
                    <br>
                    We Guarantee Our Products
                    <hr style="border-color:white;">
                    <br>
                    <h5 style="color:#999"> Received Promotions
                        You spent over US$79.00 to qualify for free shipping</h5>
                        <br>
                        <hr style="border-color:white;">
                        <br>
                        <form action="{{url('order/')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group" align="left">
                        <label style="color:white">Delivery Address</label>
                        <input type="text" class="form-control" name="daddress"/>
                        </div>
                        <div class="form-group" align="left">
                        <label style="color:white">Phone</label>
                        <input type="tel" class="form-control" name="phone" />
                        </div>
                        <input type="hidden" name="dcharges" value="2500" />
                        <input type="hidden" name="subtotal" value="{{$total}}"/>
                        <input type="hidden" name="total" value="{{$total+(($total*5)/100)+2500}}" />
                        <br>
                        <div class="form-group">
                        <input type="submit" class="form-control" style="background-color:#c52121;border-color:#c52121;color:white;" value="Check Out"/>
                        </div>
                        </form>
                        <br>
                            <p style="color:#c52121">Continue Shopping</p>
                            <br>
                </div>
            </div>
        </div>
        </div>
    </div>
<div class="separation"></div>
@endsection