@extends('layouts/customerview')

@section('content')
<div class="container p-0" align="center">
<br />
<div class="row justify-content-md-center">
<div class="col-md-5" style="height:400px">
<a href="#"><img src="{{ URL::to('/') }}/images/{{ $titleProduct->photo ?? '' }}" width="100%" height="100%" /></a>
</div>
<div class="col-md-7" style="height:400px">
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
   <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
	<li data-target="#carouselExampleIndicators" data-slide-to="1" ></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2" ></li>
  </ol>
  <div class="carousel-inner">
      <div class="carousel-item active" style="height:400px">
      <a href="#"><img class="d-block w-100 carousel-height" src="{{ URL::to('/') }}/images/{{ $slide1Product->photo ?? '' }}" alt="First slide" style="height:100%" ></a>
       
    </div>
   
        <div class="carousel-item" style="height:400px">
        <a href="#"><img class="d-block w-100 carousel-height" src="{{ URL::to('/') }}/images/{{ $slide2Product->photo ?? ''}}" alt="Second slide" style="height:100%" ></a>
       
    </div>

        <div class="carousel-item" style="height:400px">
        <a href="#"><img class="d-block w-100 carousel-height" src="{{ URL::to('/') }}/images/{{ $slide3Product->photo ?? '' }}" alt="Third slide" style="height:100%" ></a>
       
    </div>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>



</div>


</div>

<div class="separation"></div>

<div class="container-fluid">
<br />
<div class="badge" style="background-color:black;width:100%;"><h3 align="left"> &nbsp; Recommendations  <font id="subtitle" size="4" color="#666666"> &nbsp; Recommended Products for you from our Dragon Gaming</font></h3></div>
<br /><br />
<div class="row justify-content-md-center " >

<div class="col-md-3" style="height:400px;">
<a href="#"><img src="{{ URL::to('/') }}/images/{{ $rec1Product->photo ?? '' }}" width="100%" height="100%" /></a>
</div>
<div class="col-md-3" style="height:400px;">
<a href="#"><img src="{{ URL::to('/') }}/images/{{ $rec2Product->photo ?? '' }}" width="100%" height="100%" /></a>
</div>
<div class="col-md-3" style="height:400px;">
<a href="#"><img src="{{ URL::to('/') }}/images/{{ $rec3Product->photo ?? '' }}" width="100%" height="100%" /></a>
</div>
<div class="col-md-3" style="height:400px;">
<a href="#"><img src="{{ URL::to('/') }}/images/{{ $rec4Product->photo ?? '' }}" width="100%" height="100%" /></a>
</div>



</div>
</div>

<div class="separation"></div>


<div class="container">
<br />
<div class="badge" style="background-color:black;width:100%;"><h3 align="left"> &nbsp; Best Sellers <font id="subtitle" size="4" color="#666666"> &nbsp; Best Selling Products of all the time from Dragon Gaming</font></h3></div>
<br /><br />
<div class="row justify-content-md-center">
<div class="col-md-7" style="height:350px;"><a href="#"><img src="{{ URL::to('/') }}/images/{{ $best1Product->photo ?? '' }}" width="100%" height="100%" /></a></div>
<div class="col-md-5" style="height:350px;"><a href="#"><img src="{{ URL::to('/') }}/images/{{ $best2Product->photo ?? '' }}" width="100%" height="100%" /></a></div>
</div>
</div>

<div class="separation"></div>


<div class="container-fluid">
<div class="badge" style="background-color:black;width:100%;"><h3 align="left"> &nbsp; Famous Brands <font id="subtitle" size="4" color="#666666"> &nbsp; Famous Gaming brands all over the world are now available on Dragon Gaming</font></h3></div>
<br /><br />
<div class="row justify-content-md-center">
<div class="col-md-5" style="height:350px;">
<img src="{{ URL::to('/') }}/images/{{ $brand1Product->photo ?? '' }}" width="100%" height="100%" />
</div>
<div class="col-md-4" style="height:350px;">
<img src="{{ URL::to('/') }}/images/{{ $brand2Product->photo ?? '' }}" width="100%" height="100%" />
</div>
<div class="col-md-3" style="height:350px;">
<img src="{{ URL::to('/') }}/images/{{ $brand3Product->photo ?? '' }}" width="100%" height="100%" />
</div>
</div>
</div>
<br /><br />


@endsection
