@extends('layouts/app')

@section('content')
<div class="container">
<br />
<div class="row">
<div class="col-md-4">
    <div class="panel panel-default">
    <div class="panel-heading">
    <b>Brand Name : {{ $brand->name }}</b>
    </div>
    <div class="panel-body">
  
    </div>
    <div class="panel-footer">
    Total Product Count : {{ $brand->products->count() }}
    </div>
    </div>
   
    </div>
    </div>
    <div class="col-md-4" style="height:250px;">
    <img src="{{ URL::to('/') }}/images/{{ $brand->photo }}" width="100%" height="100%"/>
    </div>
    <hr />
    <table class="table table-bordered table-striped">
    <tr>
    <th>ID</th>
    <th>Product</th>
    <th>Category</th>
    <th>Status</th>
    </tr>
    @foreach($brand->products as $product)
    <tr>
    <td>{{$product->id}}</td>
     <td>{{$product->name}}</td>
      <td>{{$product->category->name}}</td>
       <td>
       <span class="{{config('dg.badge')[$product->status]}}">{{config('dg.status')[$product->status]}}
       </span>
       </td>
    </tr>
    @endforeach
    </table>    
</div>
@endsection