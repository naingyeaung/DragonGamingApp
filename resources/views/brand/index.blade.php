@extends('layouts/app')

@section('content')
  <div class="container">
    <h3>Brands List</h3>

    @if(session('info'))
    <div class="alert alert-info">
      {{ session('info') }}
    </div>
    @endif
    <table class="table table-bordered table-striped">
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>#</th>
      </tr>
      @foreach($brands as $brand)
        <tr>
          <td>{{ $brand->id }}</td>
          <td><a href="{{ url('admin/brands/view/'.$brand->id) }}">
                  {{ $brand->name }}
                  </a>
          </td>
         
          <td>
            <a href="{{ url('admin/brands/edit/'.$brand->id) }}">Edit</a>
            <a href="{{ url('admin/brands/delete/'.$brand->id) }}">Delete</a>
          </td>
        </tr>
      @endforeach
    </table>
	<a href="{{ url('admin/brands/add') }}"><button class="btn btn-primary">Add New Brand</button></a>
  </div>
@endsection