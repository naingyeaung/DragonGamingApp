@extends('layouts/app')

@section('content')
<div class="container">
  <div class="col-lg-offset-3 col-lg-6">
    <h3>Add New Brand</h3>
    @if( $errors->any() )<!--count အစား any functionကိုသံုးထားသည္-->
    <div class="alert alert-warning">
      @foreach($errors->all() as $error)
        {{ $error }}
      @endforeach
    </div>
    @endif

    <form  method="post" enctype="multipart/form-data"><!-- submit လုပ္ေသာအခါ action မပါရင္ လက္ရ်ိ route ကို post method နဲ႕သြား-->
      {{ csrf_field() }}
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" value="{{ old('name') }}" required autofocus>
        </div>
          <div class="form-group">
        <label class="custom-file">
            <input type="file" name="photo" class="custom-file-input">
            <span class="custom-file-control"></span>
        </label>
    </div>
        <input type="submit" value="Add New Brand" class="btn btn-primary">
      </div>
    </form>

  </div>

</div>
@endsection