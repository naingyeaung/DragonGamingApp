@extends('layouts/app')

@section('content')
@if(session('info'))
    <div class="alert alert-warning">
      {{session('info')}}
    </div>
    @endif
    
    
    <div class="container p-0" align="center">
<br />
<div class="row justify-content-md-center">
<div class="col-md-5" style="height:400px;" >
<img src="{{ URL::to('/') }}/images/{{ $titleProduct->photo ?? '' }}" width="100%" height="100%" />



</div>
<div class="col-md-7" style="height:400px">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
  
    <div class="item active" style="height:400px;">
      <img class="d-block w-100 carousel-height" src="{{ URL::to('/') }}/images/{{ $slide1Product->photo ?? '' }}" alt="First slide" style="height:100%" >
       
    </div>
   
        <div class="item" style="height:400px;">
      <img class="d-block w-100 carousel-height" src="{{ URL::to('/') }}/images/{{ $slide2Product->photo ?? ''}}" alt="Second slide" style="height:100%">
       
    </div>

        <div class="item" style="height:400px;">
      <img class="d-block w-100 carousel-height" src="{{ URL::to('/') }}/images/{{ $slide3Product->photo ?? '' }}" alt="Third slide" style="height:100%">
       
    </div>


  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>



</div>

<div class="row">
<div class="col-md-5">
Title
<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$titleProduct->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  @foreach($products as $product)
    <li><a href="{{url("admin/customizes/uploadProduct/title/$product->id")}}">{{$product->name}}
    @if($product->specified)
    ({{$product->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
<div class="col-md-7">
First Slide
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$slide1Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($products as $product)
    <li><a href="{{url("admin/customizes/uploadProduct/slide1/$product->id")}}">{{$product->name}}
        @if($product->specified)
    ({{$product->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>


Second Slide
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   {{$slide2Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($products as $product)
    <li><a href="{{url("admin/customizes/uploadProduct/slide2/$product->id")}}">{{$product->name}}
        @if($product->specified)
    ({{$product->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

Third Slide
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$slide3Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($products as $product)
    <li><a href="{{url("admin/customizes/uploadProduct/slide3/$product->id")}}">{{$product->name}}
        @if($product->specified)
    ({{$product->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
</div>
</div>

<div class="separation"></div>

<div class="container-fluid">
<br />
<div class="badge" style="background-color:black;width:100%;"><h3 align="left">Recommendations  <font id="subtitle" size="4" color="#666666"> &nbsp; Recommended Products for you from our Dragon Gaming</font></h3></div>
<br /><br />
<div class="row justify-content-md-center " >

<div class="col-md-3" style="height:400px;">
<img src="{{ URL::to('/') }}/images/{{ $rec1Product->photo ?? '' }}" width="100%" height="100%" />
</div>
<div class="col-md-3" style="height:400px;">
<img src="{{ URL::to('/') }}/images/{{ $rec2Product->photo ?? '' }}" width="100%" height="100%" />
</div>
<div class="col-md-3" style="height:400px;">
<img src="{{ URL::to('/') }}/images/{{ $rec3Product->photo ?? '' }}" width="100%" height="100%" />
</div>
<div class="col-md-3" style="height:400px;">
<img src="{{ URL::to('/') }}/images/{{ $rec4Product->photo ?? '' }}" width="100%" height="100%" />
</div>



</div>

<div class="row">
<div class="col-md-3">
First Recommendation
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$rec1Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($products as $product)
    <li><a href="{{url("admin/customizes/uploadProduct/rm1/$product->id")}}">{{$product->name}}
        @if($product->specified)
    ({{$product->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
<div class="col-md-3">
Second Recommendation
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$rec2Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($products as $product)
    <li><a href="{{url("admin/customizes/uploadProduct/rm2/$product->id")}}">{{$product->name}}
        @if($product->specified)
    ({{$product->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
<div class="col-md-3">
Third Recommendation
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$rec3Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($products as $product)
    <li><a href="{{url("admin/customizes/uploadProduct/rm3/$product->id")}}">{{$product->name}}
        @if($product->specified)
    ({{$product->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
<div class="col-md-3">
Fourth Recommendation
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$rec4Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($products as $product)
    <li><a href="{{url("admin/customizes/uploadProduct/rm4/$product->id")}}">{{$product->name}}
        @if($product->specified)
    ({{$product->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
</div>

</div>

<div class="separation"></div>


<div class="container">
<br />
<div class="badge" style="background-color:black;width:100%;"><h3 align="left">Best Sellers <font id="subtitle" size="4" color="#666666"> &nbsp; Best Selling Products of all the time from Dragon Gaming</font></h3></div>
<br /><br />
<div class="row justify-content-md-center">
<div class="col-md-7" style="height:350px;"><img src="{{ URL::to('/') }}/images/{{ $best1Product->photo ?? '' }}" width="100%" height="100%" /></div>
<div class="col-md-5" style="height:350px;"><img src="{{ URL::to('/') }}/images/{{ $best2Product->photo ?? '' }}" width="100%" height="100%" /></div>
</div>

<div class="row">
<div class="col-md-7">

First Bestseller
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$best1Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($products as $product)
    <li><a href="{{url("admin/customizes/uploadProduct/bs1/$product->id")}}">{{$product->name}}
        @if($product->specified)
    ({{$product->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
<div class="col-md-5">

Second Bestseller
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$best2Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($products as $product)
    <li><a href="{{url("admin/customizes/uploadProduct/bs2/$product->id")}}">{{$product->name}}
        @if($product->specified)
    ({{$product->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
</div>

</div>

<div class="separation"></div>
<br />

<div class="container-fluid">
<div class="badge" style="background-color:black;width:100%;"><h3 align="left">Famous Brands <font id="subtitle" size="4" color="#666666"> &nbsp; Famous Gaming brands all over the world are now available on Dragon Gaming</font></h3></div>
<br /><br />
<div class="row justify-content-md-center">
<div class="col-md-5" style="height:350px;">
<img src="{{ URL::to('/') }}/images/{{ $brand1Product->photo ?? '' }}" width="100%" height="100%" />
</div>
<div class="col-md-4" style="height:350px;">
<img src="{{ URL::to('/') }}/images/{{ $brand2Product->photo ?? '' }}" width="100%" height="100%" />
</div>
<div class="col-md-3" style="height:350px;">
<img src="{{ URL::to('/') }}/images/{{ $brand3Product->photo ?? '' }}" width="100%" height="100%" />
</div>
</div>

<div class="row">
<div class="col-md-5">

First Brand
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$brand1Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($brands as $brand)
    <li><a href="{{url("admin/customizes/uploadBrand/b1/$brand->id")}}">{{$brand->name}}
        @if($brand->specified)
    ({{$brand->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
<div class="col-md-4">

Second Brand
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$brand2Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($brands as $brand)
    <li><a href="{{url("admin/customizes/uploadBrand/b2/$brand->id")}}">{{$brand->name}}
        @if($brand->specified)
    ({{$brand->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
<div class="col-md-3">

Third Brand
	<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$brand3Product->name ?? ''}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
      @foreach($brands as $brand)
    <li><a href="{{url("admin/customizes/uploadBrand/b3/$brand->id")}}">{{$brand->name}}
        @if($brand->specified)
    ({{$brand->specified}})
    @endif
    </a></li>
	@endforeach
  </ul>
</div>

</div>
</div>

</div>
<div style="height:250px"></div>    
    
@endsection