@extends('layouts.customerview')

@section('content')

<div class="container">

<div class="row">

<div class="col-md-8 col-md-offset-2">

<div class="card card-default">

<div class="card-header">Purchase Confirmed</div>

<div class="card-body">

You have successfully purchased. An email is sent to you for verification.

</div>

</div>

</div>

</div>

</div>

@endsection