<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <style>
    body{
	background-color:#1D1D1D;
	font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
	}

.navbar-default {
    background-color: black;
   
}

.navbar-default .navbar-nav > li > a {
    color:#CCC;
}
.navbar-default .navbar-nav > li > a:hover,
.navbar-default .navbar-nav > li > a:focus {
    color: #333;
}
.navbar-default .navbar-nav > .active > a,
.navbar-default .navbar-nav > .active > a:hover,
.navbar-default .navbar-nav > .active > a:focus {
    color: #666;
  
}
.carousel-height{
	height:400px;
	}

.navbar-default .navbar-toggler:hover,
.navbar-default .navbar-toggler:focus {
    background-color: #333;
}
.navbar-default .navbar-toggler .navbar-toggler-icon {
     background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(187,4,4, 1)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E");
}
.btn-outline-secondary{
	background-color:#c52121;
	color:white;
	border-color:#c52121;
	
	}
.nav-link{
	color:#CCC;
	
	}	
.nav-link:hover{
	color:#666;
	}	
table tr td{
   padding:0px;
   border-color:#c52121;
}	
h3{
	font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
	color:#c52121;

	}	
.separation{
	width:100%;
	height:70px;
	}
  .priceseperation
  {
    width:100%;
    height:50px;
  }	
	
	@media (min-width: 280px) {
		#subtitle{
			display:none;
			}
			#logo{
				visibility:hidden;
				}
				.loginseperation{
	width:100%;
	height:50px;
	}
  .features-photo
  {
    height:100%;
  }
	}
	@media (min-width: 992px) {
		#subtitle{
			display:inline;
			}
			#logo{
				visibility:visible;
				}
				.loginseperation{
	width:100%;
	height:150px;
	}
  .features-photo
  {
    height:70%;
  }
	}
	.product-card{
		width:100%;
		height:100%;
		background-color:black;
		}
		.product-card2{
		width:100%;
		height:90%;
		background-color:black;
		}	
		.product-title
		{
			width:100%;
			height:50px;
				
		}
		.product-price
		{
			width:100%;
			height:130px;	
		}
    .autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}
.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff;
  border-bottom: 1px solid #d4d4d4;
}
.autocomplete-items div:hover {
  /*when hovering an item:*/
  background-color: #e9e9e9;
}
.autocomplete-active {
  /*when navigating through the items using the arrow keys:*/
  background-color: DodgerBlue !important;
  color: #ffffff;
}
.autocomplete {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
}
</style>
   
</head>
<body>
    <div id="app">
     <nav class="navbar navbar-expand-lg navbar-default">
<a class="navbar-brand" href="#"><img src="{{asset('images/logo.jpg')}}" width="30" height="30" /> <font color="#c52121;"><b>Dragon Gaming</b></font></a>


  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active pl-4">
        <a class="nav-link" href="{{url('/home/')}}"><i class="fas fa-home"></i> Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item pl-4">
        <a class="nav-link" href="{{url('/products/')}}"><i class="fas fa-gamepad"></i> Consoles</a>
      </li>
    
    
    </ul>
    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <a style="color:#CCC;"  href="{{ route('login') }}"><i style="color:#c52121;" class="fas fa-user fa-lg"></i>&nbsp;  Login&nbsp; | </a>
                            <a style="color:#CCC;;" href="{{ route('register') }}">&nbsp;  Register</a>
                        @else
                            <li class="nav-item dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                   <i style="color:#c52121;" class="fas fa-user-check"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu">
                                 
                                        <a class="dropdown-item"  href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                 
                                </div>
                            </li>
                        @endguest
                    </ul> 
   
  </div>
</nav>


<nav class="navbar navbar-expand-lg navbar-default">
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav pl-4 mr-auto">
     
      <li class="nav-item dropdown pl-5">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-list-ul"></i> Categories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        @foreach($categories as $category)
          <a class="dropdown-item" href="{{url("/productscate/$category->id")}}">{{$category->name}}</a>
          @endforeach
        </div>
      </li>
      <form action="{{url("/search/")}}" method="post">
      {{csrf_field()}}
      <li class="nav-item">
        <div class="input-group mb-3 pl-2">
        <div class="input-group-prepend">
    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
    style="background-color:white;" id="ddlconsole" >Consoles</button>
    <div class="dropdown-menu">
      <a class="dropdown-item" id="consoles" href="#" onClick="change('Consoles')">Consoles</a>
      <a class="dropdown-item" id="brands" href="#" onClick="change('Brands')">Brands</a>
      <a class="dropdown-item" id="categories" href="#" onClick="change('Categories')">Categories</a>
    </div>
  </div>

        <div class="autocomplete" style="width:200px;">

    <input id="consoleText" type="text" name="consoleText" placeholder="Search Console" class="form-control" 
    style="border-radius:0;border-left-style:solid;border-left-width:3px;border-left-color:#CCC;">
    <input id="forid" name="forid" type="hidden" />
    <input id="forType" name="forType" type="hidden" value="consoles"/>
  </div>
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
  </div>
</div>
</li>
</form>
    </ul>
    
    <a class="nav-link" href="{{ url('/promotions/') }}"><i  class="fas fa-award"></i> Promotion Area</a>
<a class="nav-link" href="#"><i  class="fas fa-user-circle"></i> Account & Lists <br> {{auth()->user()->name ??''}}</a>
<a class="nav-link" href="{{url('cart/')}}"><i  class="fas fa-shopping-cart"></i> Cart @if(Session::has('cart'))
@php $cartcount=0;
@endphp
@foreach(Session::get('cart') as $cart)
@php
$cartcount++;
@endphp   
@endforeach
<div class="badge badge-primary">{{$cartcount}}</div>
@endif
 </a>
 
  </div>
</nav>


        @yield('content')
    </div>
 <!-- Footer -->
<footer class="page-footer font-small" style="background-color:black;">

    <div style="background-color:#c52121;">
      <div class="container-fluid">

        <!-- Grid row-->
        <div class="row py-3 d-flex align-items-center">

          <!-- Grid column -->
          <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
            <h6 class="mb-0" style="color:white;">Get connected with us on social networks!</h6>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-6 col-lg-7 text-center text-md-right">

            <!-- Facebook -->
            <a class="fb-ic">
              <i style="color:white;" class="fab fa-facebook fa-lg white-text mr-4"> </i>
            </a>
            <!-- Twitter -->
            <a class="tw-ic">
              <i style="color:white;" class="fab fa-twitter fa-lg white-text mr-4"> </i>
            </a>
            <!-- Google +-->
            <a class="gplus-ic">
              <i style="color:white;" class="fab fa-google-plus fa-lg white-text mr-4"> </i>
            </a>
            <!--Linkedin -->
            <a class="li-ic">
              <i style="color:white;" class="fab fa-linkedin fa-lg white-text mr-4"> </i>
            </a>
            <!--Instagram-->
            <a class="ins-ic">
              <i style="color:white;" class="fab fa-instagram fa-lg white-text"> </i>
            </a>

          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row-->

      </div>
    </div>

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left mt-5" style="background-color:black;">

      <!-- Grid row -->
      <div class="row mt-3">

        <!-- Grid column -->
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

          <!-- Content -->
          <h6 class="text-uppercase font-weight-bold">Company name</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>Here you can use rows and columns here to organize your footer content. Lorem ipsum dolor sit amet, consectetur
            adipisicing elit.</p>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold">Products</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <a href="#!">MDBootstrap</a>
          </p>
          <p>
            <a href="#!">MDWordPress</a>
          </p>
          <p>
            <a href="#!">BrandFlow</a>
          </p>
          <p>
            <a href="#!">Bootstrap Angular</a>
          </p>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold">Useful links</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <a href="#!">Your Account</a>
          </p>
          <p>
            <a href="#!">Become an Affiliate</a>
          </p>
          <p>
            <a href="#!">Shipping Rates</a>
          </p>
          <p>
            <a href="#!">Help</a>
          </p>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold">Contact</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <i class="fa fa-home mr-3"></i> New York, NY 10012, US</p>
          <p>
            <i class="fa fa-envelope mr-3"></i> info@example.com</p>
          <p>
            <i class="fa fa-phone mr-3"></i> + 01 234 567 88</p>
          <p>
            <i class="fa fa-print mr-3"></i> + 01 234 567 89</p>

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
      <a href="https://mdbootstrap.com/bootstrap-tutorial/"> MDBootstrap.com</a>
    </div>
    <!-- Copyright -->

  </footer>
  <div style="display:none;" id="consoleData" data-permissions="{{ json_encode($products->pluck('name')) }}" data-pid="{{ json_encode($products->pluck('id')) }}"></div>
  <div style="display:none;" id="brandData" data-permissions="{{ json_encode($brands->pluck('name')) }}" data-bid="{{ json_encode($brands->pluck('id')) }}"></div>
  <div style="display:none;" id="categoryData" data-permissions="{{ json_encode($categories->pluck('name')) }}" data-cid="{{ json_encode($categories->pluck('id')) }}"></div>
  <!-- Footer -->   

    <!-- Scripts -->

      <script src="{{ asset('js/app.js') }}"></script>
      <script src="{{ asset('js/dg.js') }}"></script>
      <script src="{{ asset('js/autocomplete.js') }}"></script>
      
</body>
<script>
var permissions = $('#consoleData').data('permissions');
var pid = $('#consoleData').data('pid');
var productArray={},
   i;
for (i = 0; i < pid.length; i++) {
    productArray[pid[i]] = permissions[i];
}
autocomplete(document.getElementById("consoleText"),document.getElementById("forid"), productArray);

function change(name)
{

  $("#ddlconsole").html(name);
  if(name=='Consoles')
  {
    var permissions = $('#consoleData').data('permissions');
var pid = $('#consoleData').data('pid');
var productArray={},
   i;
for (i = 0; i < pid.length; i++) {
    productArray[pid[i]] = permissions[i];
}
autocomplete(document.getElementById("consoleText"),document.getElementById("forid"), productArray);
  $("#forType").val('consoles');
  }
  else if(name=='Brands')
  {
    var permissions = $('#brandData').data('permissions');
var pid = $('#brandData').data('bid');
var productArray={},
   i;
for (i = 0; i < pid.length; i++) {
    productArray[pid[i]] = permissions[i];
}
autocomplete(document.getElementById("consoleText"),document.getElementById("forid"), productArray);
$("#forType").val('brands');
  }
  else if(name=='Categories')
  {
    var permissions = $('#categoryData').data('permissions');
var pid = $('#categoryData').data('cid');
var productArray={},
   i;
for (i = 0; i < pid.length; i++) {
    productArray[pid[i]] = permissions[i];
}
autocomplete(document.getElementById("consoleText"),document.getElementById("forid"), productArray);
$("#forType").val('categories');
  }
}

$("#consoleText").keyup(function(){
  var curtype = $("#ddlconsole").text();
  var curvalue = $("#consoleText").val();
  
  if (curtype=='Consoles')
  {
    for (var key in productArray)
    {
      if (curvalue.toUpperCase()==productArray[key].toUpperCase())
      {
          $("#forid").val(key);
          break;
      }
      else
      {
        $("#forid").val('');
      }
    }
  }
  else if (curtype=='Brands')
  {
    var permissions = $('#brandData').data('permissions');
var pid = $('#brandData').data('bid');
var productArray={},
   i;
for (i = 0; i < pid.length; i++) {
    productArray[pid[i]] = permissions[i];
}
    for (var key in productArray)
    {
      if (curvalue.toUpperCase()==productArray[key].toUpperCase())
      {
          $("#forid").val(key);
          break;
      }
      else
      {
        $("#forid").val('');
      }
    }
  }
  if (curtype=='Categories')
  {
    var permissions = $('#categoryData').data('permissions');
var pid = $('#categoryData').data('cid');
var productArray={},
   i;
for (i = 0; i < pid.length; i++) {
    productArray[pid[i]] = permissions[i];
}
    for (var key in productArray)
    {
      if (curvalue.toUpperCase()==productArray[key].toUpperCase())
      {
          $("#forid").val(key);
          break;
      }
      else
      {
        $("#forid").val('');
      }
    }
  }
});
</script>
</html>
