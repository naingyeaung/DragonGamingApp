@extends('layouts/app')

@section('content')
<div class="container">
<h1>Products</h1>

<table class="table table-bordered table-striped">
<tr>
<th>ID</th>
<th>Name</th>
<th>Price</th>
<th>Category</th>
<th>Brand</th>
<th>Status</th>
<th>D/P</th>
<th>#</th>

</tr>
@foreach($products as $product)
<tr>
<td>{{$product->id}}</td>
<td><a href="{{ url('admin/products/view/'.$product->id) }}">{{$product->name}}</a></td>
<td>{{$product->price}}ks</td>
<td><a href="{{ url('admin/categories/view/'.$product->category->id) }}">{{$product->category->name}}</a></td>
<td><a href="{{ url('admin/brands/view/'.$product->brand->id) }}">{{$product->brand->name}}</td>
<td> <span class="{{config('dg.badge')[$product->status]}}">{{config('dg.status')[$product->status]}}
       </span></td>
    
<td>{{$product->dp ?? 'none'}}</td>
<td><a href="{{ url('admin/products/edit/'.$product->id)}}">Edit</a> <a href="{{ url('admin/products/delete/'.$product->id)}}">Delete</a></td>            
</tr>
@endforeach
</table>
{{$products->links()}}

<a href="{{ url('admin/products/add') }}"><button class="btn btn-primary">Add New Product</button></a>
@endsection
</div>