@extends('layouts/app')

@section('content')
@if(session('info'))
    <div class="alert alert-warning">
      {{session('info')}}
    </div>
    @endif
<div class="container">
 <div class="row">
 <div class="col-md-8">
 <div class="{{config('dg.panel')[$product->status]}}">
 <div class="panel-heading">
 	<b><i class="fas fa-bug"></i>{{ $product->name }} </b>
 </div>
 <div class="panel-body">
 	{{ $product->price}}ks
 </div>
 <div class="panel-footer">
 <div class="row">
 <div class="col-md-6">
 Category : {{ $product->category->name }} , Brand : {{ $product->brand->name}}
 </div>
 <div class="col-md-6 text-right">
 <a href="{{url("complains/status/$product->id/3")}}" class="btn btn-link">Out Of Stock</a>
 <a href="{{url("admin/products/edit/$product->id")}}" class="btn btn-primary"><i class="fas fa-edit"></i>Edit</a>
 <a href="{{url("admin/products/delete/$product->id")}}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
 </div>
 </div>
 </div>
 </div>
 
 <div class="row">
 <div class="col-md-6" style="height:300px;">
 <img src="{{ URL::to('/') }}/images/{{ $product->photo }}" width="100%" height="100%"/>
 </div>
 </div>
 
 <br />
 <h5>Purchasements</h5>
 <hr />
 <div class="comments">

 
 
</div> 
 </div>
 
 
 <div class="col-md-4">
 <div class="panel panel-default">
 <div class="panel-heading">
 <b>Current Status : <span class="{{config('dg.badge')[$product->status]}}">{{config('dg.status')[$product->status]}}
       </span></b>
 </div>
 <div class="panel-body">
 <form  method="post"><!-- submit လုပ္ေသာအခါ action မပါရင္ လက္ရ်ိ route ကို post method နဲ႕သြား-->
      {{ csrf_field() }}
        <div class="form-group">
        <label for="status">Status</label>
        <select id="status" name="status" class="form-control"  required>
        @foreach(config('dg.status') as $index =>$status)
     	 <option @if($index==$product->status) selected="selected" @endif value="{{$index}}">{{$status}}</option>
 		@endforeach
        </select>
        <label for="dp" id="dp_label" hidden="true"></label>
        <input type="text" name="dp" id="dp_text" hidden="true" />
        </div>
        
        
        <div class="form-group">
        <input type="submit" value="Update" class="btn btn-primary">
      </div>
    </form> 
  
 </div>
 <div class="panel-footer">
	This Product is currently {{config('dg.status')[$product->status]}} {{$product->dp ?? ''}}
 </div>
 </div>
 

 
 </div>
 </div>
    
    
</div>
@endsection