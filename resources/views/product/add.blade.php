
@extends('layouts/app')

@section('content')
<div class="container">
  <div class="col-lg-offset-3 col-lg-6">
    <h3>Add New Product</h3>
    @if( $errors->any() )<!--count အစား any functionကိုသံုးထားသည္-->
    <div class="alert alert-warning">
      @foreach($errors->all() as $error)
        {{ $error }}
      @endforeach
    </div>
    @endif

    <form  method="post" enctype="multipart/form-data"><!-- submit လုပ္ေသာအခါ action မပါရင္ လက္ရ်ိ route ကို post method နဲ႕သြား-->
      {{ csrf_field() }}
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" value="{{ old('name') }}" required autofocus>
        </div>

        <div class="form-group">
        <label for="name">Description</label>
        <textarea name="description" class="form-control" value=""></textarea>
        </div>
       
       
       <div class="form-group">
        <label for="name">Price</label>
        <input type="text" name="price" class="form-control"  required autofocus>
        </div> 
      
        <div class="form-group">
        <label for="category_id">Category</label>
        <select id="categories" name="category_id" class="form-control" required>
        <option value="0">Select Category</option>
        @foreach($categories as $category)
        <option value="{{$category->id}}" >{{$category->name}}</option>
        @endforeach
        </select>
        </div>
        
        
          <div class="form-group">
        <label for="brand_id">Brand</label>
        <select id="brands" name="brand_id" class="form-control" value="{{ old('id') }}" required>
        <option value="0">Select Brand</option>
        @foreach($brands as $brand)
        <option value="{{$brand->id}}">{{$brand->name}}</option>
        @endforeach
        </select>
        </div>
        
         <div class="form-group">
        <label for="status">Status</label>
        <select id="status" name="status" class="form-control"  required>
     	 <option value="0">Instock</option>
          <option value="1">Discounted</option>
           <option value="2">Promoted</option>
            <option value="3">Out of stock</option>
        </select>
        <label for="dp" id="dp_label" hidden="true"></label>
        <input type="text" name="dp" id="dp_text" hidden="true" />
        </div>
        
        
        <div class="form-group">
        <label class="custom-file">
            <input type="file" name="photo" class="custom-file-input">
            <span class="custom-file-control"></span>
        </label>
    </div>
        
        
        <div class="form-group">
        <input type="submit" value="Add New Product" class="btn btn-primary">
      </div>
    </form>

  </div>

</div>
@endsection