@extends('layouts/app')

@section('content')
<div class="container">
  <div class="col-lg-offset-3 col-lg-6">
    <h3>Edit Product</h3>
    @if( $errors->any() )<!--count အစား any functionကိုသံုးထားသည္-->
    <div class="alert alert-warning">
      @foreach($errors->all() as $error)
        {{ $error }}
      @endforeach
    </div>
    @endif
    <form method="post"><!-- submit လုပ္ေသာအခါ action မပါရင္ လက္ရ်ိ route ကို post method နဲ႕သြား-->
      {{ csrf_field() }}
      <div class="form-group">
        <label for="name">Subject</label>
        <input type="text" name="name" class="form-control" value="{{$product->name}}" autofocus >
        <label for="detail">Detail</label>
        <textarea name="detail" class="form-control" >{{$product->description}}</textarea>
        <label for="name">Price</label>
        <input type="text" name="price" class="form-control" value="{{$product->price}}" autofocus >
       </div>
       
       <div class="form-group">
        <label for="category_id">Category</label>
        <select id="categories" name="category_id" class="form-control" value="$product->category->name" required>
        <option value="0">Select Category</option>
        @foreach($categories as $category)
       
        <option
         @if($product->category->id==$category->id)
          selected="selected" @endif
           value="{{$category->id}}" >{{$category->name}}</option>
        
        @endforeach
        </select>
        </div>
        
        
          <div class="form-group">
        <label for="brand_id">Brand</label>
        <select id="brands" name="brand_id" class="form-control" value="$product->brand->name" required>
        <option value="0">Select Brand</option>
       @foreach($brands as $brand)
        <option  
         @if($product->brand->id==$brand->id) selected="selected" @endif 
         value="{{$brand->id}}">{{$brand->name}}</option>
        
        @endforeach
        </select>
        </div>
        
        
       <div class="form-group">
        <input type="submit" value="Update Product" class="btn btn-primary">
      </div>
    </form>

  </div>

</div>
@endsection