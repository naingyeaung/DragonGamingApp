<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport" content="height=device-height, initial-scale=1" />
        <title>Dragon Gaming</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

        <!-- Styles -->
        <style>

.container-fluid{
	padding:0;
	}
.col-sm{
	height: 100vh;
	}
.mid{
	background-size:100% 100%;
	background-image:url(images/mid-wallpaper.jpg);
	}	
.left{
	background-size:100% 100%;
	background-image:url(images/left-wallpaper.jpg);
	}
.right{
	background-size:100% 100%;
	background-image:url(images/right-wallpaper.jpg);
	}	
.wallpapper-top{
	width:100%;
	height:65vh;
	}		
.wallpapper-bot{
	width:70%;
	height:35vh;
	}	
h3{
	font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
	}		
p{
	font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
	}
@media (min-width: 280px) {
	p{
		display:none;
		}
	h3{
		color:white;
		}	
	}	
	@media (min-width: 992px) {
		h3{
			font-size:1.7em;
			color:white;
			}
			p{
				display:block;
				color:white;
				}
		}	
</style>
    </head>
    <body>
        <div class="container-fluid ">
<div class="row no-gutters">
<div class="col-sm left" align="center">
<div class="wallpapper-top">
<br />

</div>
<div class="wallpapper-bot">
<h3 align="left" ><b>Get the best gaming experience ever</b></h3>
<p align="left" ><b>What is the critical thing for best gaming exp? I might say "The Consoles".</b></p>
<br />
<a href="{{ url('/home/')}}"><h5 align="left" style="color:white; font-family:'Palatino Linotype', 'Book Antiqua', Palatino, serif">Experience</h5></a>
</div>
</div>
<div class="col-sm mid" align="center">
<div class="wallpapper-top">
<br />
<h1 align="center" style="color:white;font-family:'Palatino Linotype', 'Book Antiqua', Palatino, serif"><b>Dragon Gaming</b></h1>
</div>
<div class="wallpapper-bot">
<h3 align="left" ><b>Be a member of Dragon Gaming</b></h3>
<p align="left" ><b>Register for Dragon Gaming free and get discounts and promotions for varities of gaming consoles.</b></p>
<br />
<h5 align="left" style="color:white; font-family:'Palatino Linotype', 'Book Antiqua', Palatino, serif">Register</h5>
</div>
</div>
<div class="col-sm right" align="center">
<div class="wallpapper-top">
<br />

</div>
<div class="wallpapper-bot">
<h3 align="left" ><b>Brands are the Prooves of Quality</b></h3>
<p align="left" ><b>Check all the famous brands in consoles! They will make you qualified gamers</b></p>
<br />
<h5 align="left" style="color:white; font-family:'Palatino Linotype', 'Book Antiqua', Palatino, serif">Brands</h5>
</div>
</div>
</div>
</div>
    </body>
</html>
