@extends('layouts/app')

@section('content')
  <div class="container">
    <h3>Categories List</h3>

    @if(session('info'))
    <div class="alert alert-info">
      {{ session('info') }}
    </div>
    @endif
    <table class="table table-bordered table-striped">
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>#</th>
      </tr>
      @foreach($categories as $category)
        <tr>
          <td>{{ $category->id }}</td>
          <td><a href="{{ url('admin/categories/view/'.$category->id) }}">
                  {{ $category->name }}
                  </a>
          </td>
         
          <td>
            <a href="{{ url('admin/categories/edit/'.$category->id) }}">Edit</a>
            <a href="{{ url('admin/categories/delete/'.$category->id) }}">Delete</a>
          </td>
        </tr>
      @endforeach
    </table>
	<a href="{{ url('admin/categories/add') }}"><button class="btn btn-primary">Add New Category</button></a>
  </div>
@endsection