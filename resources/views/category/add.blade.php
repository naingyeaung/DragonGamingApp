@extends('layouts/app')

@section('content')
<div class="container">
  <div class="col-lg-offset-3 col-lg-6">
    <h3>Add New Category</h3>
    @if( $errors->any() )<!--count အစား any functionကိုသံုးထားသည္-->
    <div class="alert alert-warning">
      @foreach($errors->all() as $error)
        {{ $error }}
      @endforeach
    </div>
    @endif

    <form  method="post"><!-- submit လုပ္ေသာအခါ action မပါရင္ လက္ရ်ိ route ကို post method နဲ႕သြား-->
      {{ csrf_field() }}
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" value="{{ old('name') }}" required autofocus>
        </div>
        <input type="submit" value="Add New Category" class="btn btn-primary">
      </div>
    </form>

  </div>

</div>
@endsection