@extends('layouts/app')

@section('content')
<div class="container">
  <div class="col-lg-offset-3 col-lg-6">
    <h3>Edit Category</h3>
    @if( $errors->any() )
    <div class="alert alert-warning">
      @foreach($errors->all() as $error)
        {{ $error }}
      @endforeach
    </div>
    @endif
    <form method="post">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" value="{{$category->name}}" autofocus >
    </div>
        <input type="submit" value="Update Category" class="btn btn-primary">
      
    </form>

  </div>

</div>
@endsection