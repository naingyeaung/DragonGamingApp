@extends('layouts/app')

@section('content')
<div class="container">
<br />
<div class="row">
<div class="col-md-4">
    <div class="panel panel-default">
    <div class="panel-heading">
    <b>Category : {{ $category->name }}</b>
    </div>
    <div class="panel-body">
  
    </div>
    <div class="panel-footer">
    Total Product Count : {{ $category->products->count() }}
    </div>
    </div>
    
    </div>
    </div>
    <hr />
    <table class="table table-bordered table-striped">
    <tr>
    <th>ID</th>
    <th>Product</th>
    <th>Category</th>
    <th>Status</th>
    </tr>
    @foreach($category->products as $product)
    <tr>
    <td>{{$product->id}}</td>
     <td>{{$product->name}}</td>
      <td>{{$product->brand->name}}</td>
       <td>
       <span class="{{config('dg.badge')[$product->status]}}">{{config('dg.status')[$product->status]}}
       </span>
       </td>
    </tr>
    @endforeach
    </table>    
</div>
@endsection