<?php

return[
'status' => ['instock', 'discounted', 'promoted', 'out of stock'],
'role' =>['employee', 'manager', 'none'],
'badge' =>['label label-danger', 'label label-success', 'label label-primary',  'label label-default'],
'panel' =>['panel panel-danger', 'panel panel-success', 'panel panel-primary',  'panel panel-default']
];